Braingames is a combination of 4 intellectual games: rebuses, kubrais, hybrids, hapoifikis. 
User can choose a game and try to solve some tasks. Games are held on time and user results are saved in the standings.

Braingames is a java web project.
Tools and technologies that were used: 
- IDE Eclipse 4.12
- JDK-1.8
- Maven 3.6.1
- Apache Tomcat 9
- Spring MVC 5.x
- Spring Security 5.x
- Hibernate 5.x
- PostgreSQL 9.6

There is a start database script in the project root.

One of the project pages is presented below.

![page_example](project_screenshot.png)