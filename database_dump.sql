--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: db_braingames; Type: DATABASE; Schema: -; Owner: admin_player
--

CREATE DATABASE db_braingames WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';


ALTER DATABASE db_braingames OWNER TO admin_player;

\connect db_braingames

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: games; Type: TABLE; Schema: public; Owner: admin_player
--

CREATE TABLE games (
    id integer NOT NULL,
    type character varying NOT NULL,
    task_text character varying,
    task_reference character varying,
    answer_reference character varying,
    answer_text character varying[]
);


ALTER TABLE games OWNER TO admin_player;

--
-- Name: games_id_seq; Type: SEQUENCE; Schema: public; Owner: admin_player
--

CREATE SEQUENCE games_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE games_id_seq OWNER TO admin_player;

--
-- Name: games_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin_player
--

ALTER SEQUENCE games_id_seq OWNED BY games.id;


--
-- Name: standings; Type: TABLE; Schema: public; Owner: admin_player
--

CREATE TABLE standings (
    id integer NOT NULL,
    user_id integer NOT NULL,
    game_id integer NOT NULL,
    kit_number integer,
    "time" time without time zone NOT NULL
);


ALTER TABLE standings OWNER TO admin_player;

--
-- Name: standings_id_seq; Type: SEQUENCE; Schema: public; Owner: admin_player
--

CREATE SEQUENCE standings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE standings_id_seq OWNER TO admin_player;

--
-- Name: standings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin_player
--

ALTER SEQUENCE standings_id_seq OWNED BY standings.id;


--
-- Name: user_roles; Type: TABLE; Schema: public; Owner: admin_player
--

CREATE TABLE user_roles (
    id integer NOT NULL,
    user_id integer NOT NULL,
    role character varying NOT NULL
);


ALTER TABLE user_roles OWNER TO admin_player;

--
-- Name: user_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: admin_player
--

CREATE SEQUENCE user_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_roles_id_seq OWNER TO admin_player;

--
-- Name: user_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin_player
--

ALTER SEQUENCE user_roles_id_seq OWNED BY user_roles.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: admin_player
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying NOT NULL,
    password character varying NOT NULL
);


ALTER TABLE users OWNER TO admin_player;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: admin_player
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO admin_player;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin_player
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: games id; Type: DEFAULT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY games ALTER COLUMN id SET DEFAULT nextval('games_id_seq'::regclass);


--
-- Name: standings id; Type: DEFAULT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY standings ALTER COLUMN id SET DEFAULT nextval('standings_id_seq'::regclass);


--
-- Name: user_roles id; Type: DEFAULT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY user_roles ALTER COLUMN id SET DEFAULT nextval('user_roles_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: games; Type: TABLE DATA; Schema: public; Owner: admin_player
--

COPY games (id, type, task_text, task_reference, answer_reference, answer_text) FROM stdin;
11	kubra	инженер глупость	\N	\N	{техникум}
9	rebus	\N	7.png	\N	{мелодрама}
12	kubra	норд крыло	\N	\N	{острог}
13	kubra	господин к эта	\N	\N	{работа}
10	rebus	\N	8.png	\N	{торнадо}
19	hapoifika	ГоИПр	\N	\N	{гордостьипредубеждение}
20	hapoifika	ФорГа	\N	\N	{форрестгамп}
21	hapoifika	ОДо	\N	\N	{одиндома}
22	hapoifika	ПрНаГнКу	\N	\N	{пролетаянадгнездомкукушки}
23	hapoifika	УмУиХ	\N	\N	{умницауиллхантинг}
24	hapoifika	ЗаИМеД	\N	\N	{запискиизмертвогодома}
25	hapoifika	КрОт	\N	\N	{крестныйотец}
17	kubra	пара ты мясо	\N	\N	{двоякость}
26	hapoifika	ИДжИПоКрПо	\N	\N	{индианаджонсипоследнийкрестовыйпоход}
18	kubra	пробел спины	\N	\N	{таблица}
14	kubra	на носика	\N	\N	{подушка}
15	kubra	кря кря сом против	\N	\N	{кукуруза}
1	hybrid	\N	1.png	\N	{фотографинад}
16	kubra	про лес	\N	\N	{перебор}
32	hybrid	\N	7.png	\N	{оскарета}
27	hybrid	\N	2.png	\N	{бамбукварь}
28	hybrid	\N	3.png	\N	{табунт}
3	rebus	\N	1.png	\N	{аннотация}
29	hybrid	\N	4.png	\N	{искратер}
4	rebus	\N	2.png	\N	{компромисс}
33	hybrid	\N	8.png	\N	{стеклоун}
5	rebus	\N	3.png	\N	{кредо}
6	rebus	\N	4.png	\N	{мизансцена}
7	rebus	\N	5.png	\N	{симбиоз}
8	rebus	\N	6.png	\N	{субсидия}
30	hybrid	\N	5.png	\N	{парапландыш}
31	hybrid	\N	6.png	\N	{кораллергия}
\.


--
-- Name: games_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin_player
--

SELECT pg_catalog.setval('games_id_seq', 33, true);


--
-- Data for Name: standings; Type: TABLE DATA; Schema: public; Owner: admin_player
--

COPY standings (id, user_id, game_id, kit_number, "time") FROM stdin;
\.


--
-- Name: standings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin_player
--

SELECT pg_catalog.setval('standings_id_seq', 1, false);


--
-- Data for Name: user_roles; Type: TABLE DATA; Schema: public; Owner: admin_player
--

COPY user_roles (id, user_id, role) FROM stdin;
2	2	ADMIN
3	2	USER
4	4	USER
15	6	USER
18	6	ADMIN
\.


--
-- Name: user_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin_player
--

SELECT pg_catalog.setval('user_roles_id_seq', 19, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: admin_player
--

COPY users (id, username, password) FROM stdin;
2	admin	admin
4	player2	play
6	horse234	horse
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin_player
--

SELECT pg_catalog.setval('users_id_seq', 6, true);


--
-- Name: games games_pkey; Type: CONSTRAINT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY games
    ADD CONSTRAINT games_pkey PRIMARY KEY (id);


--
-- Name: standings standings_pkey; Type: CONSTRAINT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY standings
    ADD CONSTRAINT standings_pkey PRIMARY KEY (id);


--
-- Name: user_roles user_role_unique; Type: CONSTRAINT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT user_role_unique UNIQUE (user_id, role);


--
-- Name: user_roles user_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT user_roles_pkey PRIMARY KEY (id);


--
-- Name: users username_unique; Type: CONSTRAINT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY users
    ADD CONSTRAINT username_unique UNIQUE (username);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- Name: standings standings_game_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY standings
    ADD CONSTRAINT standings_game_id_fkey FOREIGN KEY (game_id) REFERENCES games(id) ON DELETE CASCADE;


--
-- Name: standings standings_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY standings
    ADD CONSTRAINT standings_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: user_roles user_roles_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin_player
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT user_roles_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

