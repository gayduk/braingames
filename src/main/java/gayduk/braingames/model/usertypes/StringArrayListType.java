package gayduk.braingames.model.usertypes;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

public class StringArrayListType implements UserType {

  @Override
  public int[] sqlTypes() {
    return new int[]{Types.ARRAY};
  }
    
  @Override
  @SuppressWarnings("rawtypes")
  public Class<ArrayList> returnedClass() {
    return ArrayList.class;
  }

  @Override
  public Object nullSafeGet(
      ResultSet rs, String[] names, 
      SharedSessionContractImplementor session, Object o) 
          throws HibernateException, SQLException {
    Array array = rs.getArray(names[0]);
    if (array == null) {
      return null;
    }
    String[] javaArray = (String[])array.getArray();
    ArrayList<String> result = new ArrayList<>();
    Collections.addAll(result, javaArray);
    return result;
  }

  @Override
  public void nullSafeSet(
      PreparedStatement st, 
      Object value, 
      int index, 
      SharedSessionContractImplementor session) 
          throws HibernateException, SQLException {
    Connection conn = st.getConnection();
    if (value == null) {
      st.setNull(index, sqlTypes()[0]);
    } else {
      @SuppressWarnings("unchecked") ArrayList<Long> castObject = (ArrayList<Long>) value;
      String[] strings = castObject.toArray(new String[castObject.size()]);
      Array array = conn.createArrayOf("varchar", strings);
      st.setArray(index, array);
    }
  }

  @Override
  public Object assemble(Serializable cached, Object owner) throws HibernateException {
    return cached;
  }

  @Override
  @SuppressWarnings("unchecked")
  public Object deepCopy(Object o) throws HibernateException {
    return o == null ? null : ((ArrayList<Object>) o).clone();
  }

  @Override
  public Serializable disassemble(Object o) throws HibernateException {
    return (Serializable)o;
  }

  @Override
  public boolean isMutable() {
    return false;
  }

  @Override
  public Object replace(Object original, Object target, Object owner) throws HibernateException {
    return original;
  }

  @Override
  public boolean equals(Object x, Object y) throws HibernateException {
    return x == y;
  }

  @Override
  public int hashCode(Object x) throws HibernateException {
    return x.hashCode();
  }
}
