package gayduk.braingames.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(UserRole.class)
public class UserRole_ {
  public static volatile SingularAttribute<UserRole, Integer> id;
  public static volatile SingularAttribute<UserRole, String> role;
  public static volatile SingularAttribute<UserRole, User> user;
}
