package gayduk.braingames.model;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(User.class)
public class User_ {
  public static volatile SingularAttribute<User, Integer> id;
  public static volatile SingularAttribute<User, String> userName;
  public static volatile SingularAttribute<User, String> password;
  public static volatile ListAttribute<User, UserRole> roles;
  public static volatile ListAttribute<User, Standings> standings;
}
