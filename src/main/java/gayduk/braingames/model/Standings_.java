package gayduk.braingames.model;

import java.sql.Time;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Standings.class)
public class Standings_ {
  public static volatile SingularAttribute<Standings, Integer> id;
  public static volatile SingularAttribute<Standings, Time> time;
  public static volatile SingularAttribute<Standings, Integer> kitNumber;
  public static volatile SingularAttribute<Standings, User> user;
  public static volatile SingularAttribute<Standings, Game> game;
}
