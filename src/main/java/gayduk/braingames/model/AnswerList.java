package gayduk.braingames.model;

import java.util.List;

public class AnswerList {
  private List<String> answerList;

  public List<String> getAnswerList() {
    return answerList;
  }

  public void setAnswerList(List<String> answerList) {
    this.answerList = answerList;
  }
}
