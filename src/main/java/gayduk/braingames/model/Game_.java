package gayduk.braingames.model;

import java.util.ArrayList;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Game.class)
public class Game_ {
  public static volatile SingularAttribute<Game, Integer> id;
  public static volatile SingularAttribute<Game, String> type;
  public static volatile SingularAttribute<Game, String> kitNumber;
  public static volatile SingularAttribute<Game, String> taskText;
  public static volatile SingularAttribute<Game, String> taskReference;
  public static volatile SingularAttribute<Game, ArrayList<String>> answerText;
  public static volatile SingularAttribute<Game, String> answerReference;
  public static volatile ListAttribute<Game, Standings> standings;
}
