package gayduk.braingames.model;

import gayduk.braingames.model.usertypes.StringArrayListType;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

@TypeDefs({
    @TypeDef(
        name = "string-array-list",
        typeClass = StringArrayListType.class
    )
})
@Entity
@Table(name = "games")
public class Game {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "type", nullable = false)
  private String type;

  @Column(name = "task_text")
  private String taskText;

  @Column(name = "task_reference")
  private String taskReference;
    
  @Type(type = "string-array-list")
  @Column(name = "answer_text")
  private ArrayList<String> answerText;

  @Column(name = "answer_reference")
  private String answerReference;
    
  @OneToMany(mappedBy = "game", cascade = CascadeType.ALL, 
      orphanRemoval = true, fetch = FetchType.EAGER)
  private List<Standings> standings;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getTaskText() {
    return taskText;
  }

  public void setTaskText(String taskText) {
    this.taskText = taskText;
  }

  public String getTaskReference() {
    return taskReference;
  }

  public void setTaskReference(String taskReference) {
    this.taskReference = taskReference;
  }
    
  public ArrayList<String> getAnswerText() {
    return answerText;
  }

  public void setAnswerText(ArrayList<String> answerText) {
    this.answerText = answerText;
  }

  public String getAnswerReference() {
    return answerReference;
  }

  public void setAnswerReference(String answerReference) {
    this.answerReference = answerReference;
  }
    
  public List<Standings> getStandings() {
    return standings;
  }

  public void setStandings(List<Standings> standings) {
    this.standings = standings;
  }

  @Override
  public String toString() {
    return type;
  }
}
