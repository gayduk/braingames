package gayduk.braingames.model;

import java.sql.Time;

import javax.persistence.*;

@Entity
@Table(name = "standings")
public class Standings {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "time", nullable = false)
  private Time time;

  @Column(name = "kit_number")
  private int kitNumber;

  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;

  @ManyToOne
  @JoinColumn(name = "game_id")
  private Game game;

  public Standings() {
  }

  public Standings(Time time, int kitNumber, User user, Game game) {
    this.time = time;
    this.kitNumber = kitNumber;
    this.user = user;
    this.game = game;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Time getTime() {
    return time;
  }

  public void setTime(Time time) {
    this.time = time;
  }

  public int getKitNumber() {
    return kitNumber;
  }

  public void setKitNumber(int kitNumber) {
    this.kitNumber = kitNumber;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Game getGame() {
    return game;
  }

  public void setGame(Game game) {
    this.game = game;
  }

}
