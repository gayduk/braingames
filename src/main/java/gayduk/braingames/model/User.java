package gayduk.braingames.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "username", nullable = false)
  private String userName;

  @Column(name = "password", nullable = false)
  private String password;

  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, 
      orphanRemoval = true, fetch = FetchType.EAGER)
  private List<UserRole> roles;

  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Standings> standings;

  public User() {
    roles = new ArrayList<>();
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public List<Standings> getStandings() {
    return standings;
  }

  public void setStandings(List<Standings> standings) {
    this.standings = standings;
  }

  public List<UserRole> getRoles() {
    return roles;
  }

  public void setRoles(List<UserRole> roles) {
    this.roles = roles;
  }

  public void addRole(String roleName) {
    for (UserRole role : this.roles) {
      if (role.getRole().equals(roleName)) {
        return;
      }
    }
    this.roles.add(new UserRole(roleName, this));
  }

  public void removeRole(String roleName) {
    for (UserRole role : this.roles) {
      if (role.getRole().equals(roleName)) {
        this.roles.remove(role);
        return;
      }
    }
  }

  public List<String> getRolesNames() {
    List<String> rolesNames = new ArrayList<>();
    this.roles.forEach(role -> rolesNames.add(role.getRole()));
    return rolesNames;
  }
}
