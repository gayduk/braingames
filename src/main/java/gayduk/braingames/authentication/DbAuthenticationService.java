package gayduk.braingames.authentication;

import gayduk.braingames.model.User;
import gayduk.braingames.service.interfaces.IUserService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class DbAuthenticationService implements UserDetailsService {
  private IUserService userService;

  @Autowired
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    BCryptPasswordEncoder encoder = passwordEncoder();
    User user = userService.getByName(username); 
    if (user == null) {
      throw new UsernameNotFoundException("User " + username + " was not found in the database");
    }
    return new org.springframework.security.core.userdetails.User(
        user.getUserName(), encoder.encode(user.getPassword()), 
        this.getGrantedAuthorities(user));
  }
    
  private List<GrantedAuthority> getGrantedAuthorities(User user) {
    // [USER,ADMIN,..]
    List<String> roles = user.getRolesNames();
    List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
    if (!roles.isEmpty()) {
      for (String role: roles)  {
        // ROLE_USER, ROLE_ADMIN,..
        GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + role);
        grantList.add(authority);
      }
    }
    return grantList;
  }
}
