package gayduk.braingames.dao;

import gayduk.braingames.dao.interfaces.IGameDAO;
import gayduk.braingames.model.Game;
import gayduk.braingames.model.Game_;
import gayduk.braingames.model.Standings;
import gayduk.braingames.model.Standings_;
import gayduk.braingames.model.User;
import gayduk.braingames.model.User_;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class GameDAO implements IGameDAO {
  private SessionFactory sessionFactory;

  @Autowired
  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Override
  public void create(Game game) {
    Session session = sessionFactory.getCurrentSession();
    session.persist(game);
  }

  @Override
  public void update(Game game) {
    Session session = sessionFactory.getCurrentSession();
    session.update(game);
  }

  @Override
  public void delete(Game game) {
    Session session = sessionFactory.getCurrentSession();
    session.delete(game);
  }
    
  @Override
  @SuppressWarnings("unchecked")    
  public List<Game> getAllGames() {
    Session session = sessionFactory.getCurrentSession();
    return session.createQuery("from Game").list();
  }

  @Override
  public Game getById(int id) {
    Session session = sessionFactory.getCurrentSession();
    return session.get(Game.class, id);
  }
    
  @Override
  public List<Game> getGameKit(String gameType, int userId, int kitSize) {
    Session session = sessionFactory.getCurrentSession();
    CriteriaBuilder builder = session.getCriteriaBuilder();
    CriteriaQuery<Game> cq = builder.createQuery(Game.class);
    Root<Game> rootGame = cq.from(Game.class);

    Subquery<Integer> subQuery = cq.subquery(Integer.class);
    Root<Game> subRootGame = subQuery.from(Game.class);
    Join<Game, Standings> gameStandings = subRootGame.join(Game_.standings);
    Join<Standings, User> standingsUser = gameStandings.join(Standings_.user);
    subQuery.select(subRootGame.get(Game_.id));
    subQuery.where(builder.equal(standingsUser.get(User_.id), userId));

    Predicate userRestriction = builder.not(builder.in(rootGame.get(Game_.id)).value(subQuery));
    Predicate gameTypeRestriction = builder.equal(rootGame.get(Game_.type), gameType);
    cq.where(builder.and(userRestriction, gameTypeRestriction));
    cq.orderBy(builder.asc(rootGame.get(Game_.id)));
    cq.distinct(true);
    TypedQuery<Game> query = session.createQuery(cq).setMaxResults(kitSize);
    List<Game> games = query.getResultList();

    return games;
  }
    
}