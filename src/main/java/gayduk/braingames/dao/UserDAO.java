package gayduk.braingames.dao;

import gayduk.braingames.dao.interfaces.IUserDAO;
import gayduk.braingames.model.User;
import gayduk.braingames.model.UserRole;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO implements IUserDAO {
  private SessionFactory sessionFactory;

  @Autowired
  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Override
  public void create(User user) {
    for (UserRole role : user.getRoles()) {
      role.setUser(user);
    }
    Session session = sessionFactory.getCurrentSession();
    session.persist(user);
  }

  @Override
  public void update(User user) {
    Session session = sessionFactory.getCurrentSession();
    session.update(user);
  }

  @Override
  public void delete(User user) {
    Session session = sessionFactory.getCurrentSession();
    session.delete(user);
  }
  
  @Override
  @SuppressWarnings("unchecked")    
  public List<User> getAllUsers() {
    Session session = sessionFactory.getCurrentSession();
    return session.createQuery("from User").list();
  }
    
  @Override
  public User getById(int id) {
    Session session = sessionFactory.getCurrentSession();
    return session.get(User.class, id);
  }
    
  @Override
  public User getByName(String username) {
    Session session = sessionFactory.getCurrentSession();
    CriteriaBuilder cb = session.getCriteriaBuilder();
    CriteriaQuery<User> cr = cb.createQuery(User.class);
    Root<User> root = cr.from(User.class);
    cr.select(root).where(cb.equal(root.get("userName"), username));
    Query<User> query = session.createQuery(cr);
    User user = query.getSingleResult();
    return user;
  }
}
