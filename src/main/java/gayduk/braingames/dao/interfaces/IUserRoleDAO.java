package gayduk.braingames.dao.interfaces;

import gayduk.braingames.model.UserRole;

import java.util.List;

public interface IUserRoleDAO {
  void create(UserRole role);
  
  void update(UserRole role);
  
  void delete(UserRole role);
  
  List<UserRole> getAllUsersRoles();
  
  UserRole getById(int id);
}
