package gayduk.braingames.dao.interfaces;

import gayduk.braingames.model.Game;

import java.util.List;

public interface IGameDAO {
  void create(Game game);
  
  void update(Game game);
  
  void delete(Game game);
  
  List<Game> getAllGames();
  
  Game getById(int id);
  
  List<Game> getGameKit(String gameType, int userId, int kitSize);
}
