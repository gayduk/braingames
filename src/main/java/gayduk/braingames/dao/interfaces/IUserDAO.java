package gayduk.braingames.dao.interfaces;

import gayduk.braingames.model.User;

import java.util.List;

public interface IUserDAO {
  void create(User user);
  
  void update(User user);
  
  void delete(User user);
  
  List<User> getAllUsers();
  
  User getById(int id);
  
  User getByName(String username);
}
