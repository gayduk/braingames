package gayduk.braingames.dao.interfaces;

import gayduk.braingames.model.Standings;
import gayduk.braingames.model.User;

import java.util.List;

public interface IStandingsDAO {
  void create(Standings standings);
  
  void update(Standings standings);
  
  void delete(Standings standings);
  
  List<Standings> getAllStandings();
  
  Standings getById(int id);
  
  int getNextKitNumber(User user, String gameType);
}
