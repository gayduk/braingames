package gayduk.braingames.dao;

import gayduk.braingames.dao.interfaces.IStandingsDAO;
import gayduk.braingames.model.Game;
import gayduk.braingames.model.Game_;
import gayduk.braingames.model.Standings;
import gayduk.braingames.model.Standings_;
import gayduk.braingames.model.User;
import gayduk.braingames.model.User_;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StandingsDAO implements IStandingsDAO {
  private SessionFactory sessionFactory;

  @Autowired
  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Override
  public void create(Standings standings) {
    Session session = sessionFactory.getCurrentSession();
    session.persist(standings);
  }

  @Override
  public void update(Standings standings) {
    Session session = sessionFactory.getCurrentSession();
    session.update(standings);
  }

  @Override
  public void delete(Standings standings) {
    Session session = sessionFactory.getCurrentSession();
    session.delete(standings);
  }
  
  @Override
  public List<Standings> getAllStandings() {
    Session session = sessionFactory.getCurrentSession();
    CriteriaBuilder builder = session.getCriteriaBuilder();
    
    CriteriaQuery<Standings> cq = builder.createQuery(Standings.class);
    Root<Standings> rootStandings = cq.from(Standings.class);
    Join<Standings, User> standingsUser = rootStandings.join(Standings_.user);
    List<Order> orderList = new ArrayList<>();
    orderList.add(builder.asc(rootStandings.get(Standings_.time)));
    orderList.add(builder.asc(standingsUser.get(User_.id)));
    orderList.add(builder.asc(rootStandings.get(Standings_.kitNumber)));
    cq.orderBy(orderList);
    Query<Standings> query = session.createQuery(cq);
    List<Standings> standingsList = query.getResultList();
    return standingsList;
  }
    
  @Override
  public Standings getById(int id) {
    Session session = sessionFactory.getCurrentSession();
    return session.get(Standings.class, id);
  }
  
  /**
   * Figure out number of the next game kit.
   * 
   * @param user      user-player
   * @param gameType  game type
   * @return          number of game kit 
   */
  public int getNextKitNumber(User user, String gameType) {
    Session session = sessionFactory.getCurrentSession();
    CriteriaBuilder builder = session.getCriteriaBuilder();

    CriteriaQuery<Integer> cq = builder.createQuery(Integer.class);
    Root<Game> rootGame = cq.from(Game.class);
    Join<Game, Standings> gameStandings = rootGame.join(Game_.standings);
    Join<Standings, User> standingsUser = gameStandings.join(Standings_.user);
    cq.select(builder.max(gameStandings.get(Standings_.kitNumber)));
    cq.where(builder.and(builder.equal(standingsUser.get(User_.id), user.getId()),
        builder.equal(rootGame.get(Game_.type), gameType)));
    Query<Integer> query = session.createQuery(cq);
    Integer maxNumber = query.getSingleResult();

    if (maxNumber == null) {
      return 1;
    }
    return maxNumber + 1;
  }
    
}
