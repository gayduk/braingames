package gayduk.braingames.dao;

import gayduk.braingames.dao.interfaces.IUserRoleDAO;
import gayduk.braingames.model.UserRole;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRoleDAO implements IUserRoleDAO {
  private SessionFactory sessionFactory;

  @Autowired
  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Override
  public void create(UserRole role) {
    Session session = sessionFactory.getCurrentSession();
    session.persist(role);
  }

  @Override
  public void update(UserRole role) {
    Session session = sessionFactory.getCurrentSession();
    session.update(role);
  }

  @Override
  public void delete(UserRole role) {
    Session session = sessionFactory.getCurrentSession();
    session.delete(role);
  }

  @Override
  @SuppressWarnings("unchecked")    
  public List<UserRole> getAllUsersRoles() {
    Session session = sessionFactory.getCurrentSession();
    return session.createQuery("from UserRole").list();
  }
    
  @Override
  public UserRole getById(int id) {
    Session session = sessionFactory.getCurrentSession();
    return session.get(UserRole.class, id);
  }
}
