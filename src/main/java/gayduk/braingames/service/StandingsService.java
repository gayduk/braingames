package gayduk.braingames.service;

import gayduk.braingames.dao.interfaces.IStandingsDAO;
import gayduk.braingames.model.Game;
import gayduk.braingames.model.Standings;
import gayduk.braingames.model.User;
import gayduk.braingames.service.interfaces.IStandingsService;

import java.sql.Time;
import java.time.LocalTime;
import java.util.List;
import java.util.ListIterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StandingsService implements IStandingsService {
  private IStandingsDAO standingsDAO;

  @Autowired
  public void setStandingsDAO(IStandingsDAO standingsDAO) {
    this.standingsDAO = standingsDAO;
  }

  @Override
  @Transactional
  public void create(Standings standings) {
    standingsDAO.create(standings);
  }
  
  @Override
  @Transactional
  public void create(User user, List<Game> gameKit, LocalTime time) {
    String gameType = gameKit.get(0).getType();
    int kitNumber = standingsDAO.getNextKitNumber(user, gameType);
    for (Game game : gameKit) {
      standingsDAO.create(
          new Standings(Time.valueOf(time), kitNumber, user, game));
    }
  }

  @Override
  @Transactional
  public void update(Standings standings) {
    standingsDAO.update(standings);
  }

  @Override
  @Transactional
  public void delete(Standings standings) {
    standingsDAO.delete(standings);
  }

  @Override
  @Transactional
  public List<Standings> getAllStandings() {
    return standingsDAO.getAllStandings();
  }

  @Override
  @Transactional
  public Standings getById(int id) {
    return standingsDAO.getById(id);
  }

  @Override
  @Transactional
  public List<Standings> getAllGameKit() {
    List<Standings> games = standingsDAO.getAllStandings();
    ListIterator<Standings> iterator = games.listIterator();
    while (iterator.hasNext()) {
      Standings standings = iterator.next();
      if (iterator.hasNext()) {
        Standings nextStandings = iterator.next();
        if (standings.getUser().getId() == nextStandings.getUser().getId()
            && standings.getGame().getType().equals(nextStandings.getGame().getType())
            && standings.getKitNumber() == nextStandings.getKitNumber()) {
          iterator.remove();
        }
        iterator.previous();
      }
    }
    return games;
  }
}
