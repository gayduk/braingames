package gayduk.braingames.service;

import gayduk.braingames.dao.interfaces.IUserDAO;
import gayduk.braingames.model.User;
import gayduk.braingames.model.UserRole;
import gayduk.braingames.service.interfaces.IUserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService implements IUserService {
  private IUserDAO userDAO;
    
  @Autowired
  public void setUserDAO(IUserDAO userDAO) {
    this.userDAO = userDAO;
  }

  @Override
  @Transactional
  public void create(User user) {
    userDAO.create(user);
  }
    
  @Override
  @Transactional
  public void create(User user, String[] roles) {
    this.create(user);
    for (String role : Arrays.asList(roles)) {
      user.addRole(role);
    }
  }

  @Override
  @Transactional
  public void update(User user) {
    userDAO.update(user);
  }
    
  @Override
  @Transactional
  public void update(User upUser, String[] upRoles) {
    User user = this.getById(upUser.getId());
    if (!user.getUserName().equals(upUser.getUserName())) {
      user.setUserName(upUser.getUserName());
    }
    if (!user.getPassword().equals(upUser.getPassword())) {
      user.setPassword(upUser.getPassword());
    }

    List<String> newRoles = new ArrayList<>(Arrays.asList(upRoles));
    List<String> removeRoles = new ArrayList<>();
    for (UserRole role : user.getRoles()) {
      boolean isRoleForRemove = true;
      Iterator<String> iterator = newRoles.iterator();
      while (iterator.hasNext()) {
        String upRole = iterator.next();
        if (upRole.equals(role.getRole())) {
          iterator.remove();
          isRoleForRemove = false;
          break;
        }
      }
      if (isRoleForRemove) {
        removeRoles.add(role.getRole());
      }
    }
    removeRoles.forEach(role -> user.removeRole(role));
    newRoles.forEach(role -> user.addRole(role));
  }

  @Override
  @Transactional
  public void delete(User user) {
    userDAO.delete(user);
  }

  @Override
  @Transactional
  public List<User> getAllUsers() {
    return userDAO.getAllUsers();
  }

  @Override
  @Transactional
  public User getById(int id) {
    return userDAO.getById(id);
  }
    
  @Override
  @Transactional
  public User getByName(String username) {
    return userDAO.getByName(username);
  }

}
