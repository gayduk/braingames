package gayduk.braingames.service.interfaces;

import gayduk.braingames.model.Game;
import gayduk.braingames.model.Standings;
import gayduk.braingames.model.User;

import java.time.LocalTime;
import java.util.List;

public interface IStandingsService {
  void create(Standings standings);
  
  void create(User user, List<Game> gameKit, LocalTime time);
  
  void update(Standings standings);
  
  void delete(Standings standings);
  
  List<Standings> getAllStandings();
  
  Standings getById(int id);
  
  List<Standings> getAllGameKit();
}
