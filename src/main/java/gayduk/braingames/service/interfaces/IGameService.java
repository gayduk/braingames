package gayduk.braingames.service.interfaces;

import gayduk.braingames.model.Game;

import java.util.List;

public interface IGameService {
  int getKitSize();
  
  void create(Game game);
  
  void update(Game game);
  
  void delete(Game game);
  
  List<Game> getAllGames();
  
  Game getById(int id);
  
  List<Game> getGameKit(String gameType, int userId);
}
