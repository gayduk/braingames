package gayduk.braingames.service.interfaces;

import gayduk.braingames.model.User;

import java.util.List;

public interface IUserService {
  void create(User user);
  
  void create(User user, String[] roles);
  
  void update(User user);
  
  void update(User upUser, String[] upRoles);
  
  void delete(User user);
  
  List<User> getAllUsers();
  
  User getById(int id);
  
  User getByName(String username);
  
}
