package gayduk.braingames.service;

import gayduk.braingames.dao.interfaces.IGameDAO;
import gayduk.braingames.model.Game;
import gayduk.braingames.service.interfaces.IGameService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GameService implements IGameService {
  private IGameDAO gameDAO;
  private final int KIT_SIZE = 8; 
    
  @Autowired
  public void setGameDAO(IGameDAO gameDAO) {
    this.gameDAO = gameDAO;
  }

  public int getKitSize() {
    return KIT_SIZE;
  }

  @Override
  @Transactional
  public void create(Game game) {
    gameDAO.create(game);
  }

  @Override
  @Transactional
  public void update(Game game) {
    gameDAO.update(game);
  }

  @Override
  @Transactional
  public void delete(Game game) {
    gameDAO.delete(game);
  }

  @Override
  @Transactional
  public List<Game> getAllGames() {
    return gameDAO.getAllGames();
  }
    
  @Override
  @Transactional
  public Game getById(int id) {
    return gameDAO.getById(id);
  }
    
  @Override
  @Transactional
  public List<Game> getGameKit(String gameType, int userId) {
    return gameDAO.getGameKit(gameType, userId, this.KIT_SIZE);
  }

}
