package gayduk.braingames.controller;

import gayduk.braingames.model.Standings;
import gayduk.braingames.service.interfaces.IStandingsService;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {
  private IStandingsService standingsService;

  @Autowired
  public void setStandingsService(IStandingsService standingsService) {
    this.standingsService = standingsService;
  }

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public ModelAndView homePage() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("homePage");
    return modelAndView;
  }
    
  @RequestMapping(value = "/gametypes-descriptions", method = RequestMethod.GET)
  public ModelAndView gameTypesDescriptionsPage() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("gameTypesDescriptionsPage");
    return modelAndView;
  }
    
  @RequestMapping(value = "/standings", method = RequestMethod.GET)
  public ModelAndView standingsPage() {
    List<Standings> standings = standingsService.getAllGameKit();
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("standingsPage");
    modelAndView.addObject("standingsList", standings);
    return modelAndView;
  }
    
  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public ModelAndView loginPage() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("loginPage");
    return modelAndView;
  }
    
  @RequestMapping(value = "/loginSuccessful", method = RequestMethod.GET)
  public ModelAndView loginSuccessfulPage() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("loginSuccessfulPage");
    return modelAndView;
  }
  
  @RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
  public ModelAndView logoutSuccessfulPage() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("logoutSuccessfulPage");
    return modelAndView;
  }
    
  @RequestMapping(value = "/403", method = RequestMethod.GET)
  public ModelAndView accessDenied(Principal principal) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("403Page");
    if (principal != null) {
      modelAndView.addObject("welcome", "������, " + principal.getName() + "!<br>");
      modelAndView.addObject("message", "�� �� ������ ����� ������� � ���� ��������, ������(:");
    } else {
      modelAndView.addObject("welcome", "������, ����������!<br>");
      modelAndView.addObject("message", "�� �� ������ ����� ������� � ���� ��������. "
          + "�������� ��������������, ����� �������");
    }
    return modelAndView;
  }
}
