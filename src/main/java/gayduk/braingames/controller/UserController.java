package gayduk.braingames.controller;

import gayduk.braingames.model.User;
import gayduk.braingames.service.interfaces.IUserService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
  private IUserService userService;

  @Autowired
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  @RequestMapping(value = "/users", method = RequestMethod.GET)
  public ModelAndView users() {
    List<User> users = userService.getAllUsers();
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("usersPage");
    modelAndView.addObject("usersList", users);
    return modelAndView;
  }
    
  @RequestMapping(value = "/users/edit/{id}", method = RequestMethod.GET)
  public ModelAndView editPage(@PathVariable("id") int id) {
    User user = userService.getById(id);
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("editUserPage");
    modelAndView.addObject("user", user);
    return modelAndView;
  }
    
  @RequestMapping(value = "/users/edit", method = RequestMethod.POST)
  public ModelAndView editUser(@ModelAttribute("user") User upUser, 
      @RequestParam(value = "userRoles") String[] upRoles) {

    userService.update(upUser, upRoles);
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("redirect:/users");
    return modelAndView;
  }
    
  @RequestMapping(value = "/users/add", method = RequestMethod.GET)
  public ModelAndView addPage() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("editUserPage");
    return modelAndView;
  }
    
  @RequestMapping(value = "/users/add", method = RequestMethod.POST)
  public ModelAndView addUser(
      @ModelAttribute("user") User user, 
      @RequestParam(value = "userRoles") String[] roles) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("redirect:/users");
    userService.create(user, roles);
    return modelAndView;
  }
    
  @RequestMapping(value = "/users/delete/{id}", method = RequestMethod.GET)
  public ModelAndView deleteUser(@PathVariable("id") int id) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("redirect:/users");
    User user = userService.getById(id);
    userService.delete(user);
    return modelAndView;
  }
}
