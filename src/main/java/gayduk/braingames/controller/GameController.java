package gayduk.braingames.controller;

import gayduk.braingames.model.AnswerList;
import gayduk.braingames.model.Game;
import gayduk.braingames.model.User;
import gayduk.braingames.service.interfaces.IGameService;
import gayduk.braingames.service.interfaces.IStandingsService;
import gayduk.braingames.service.interfaces.IUserService;

import java.security.Principal;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GameController {
  private IGameService gameService;
  private IUserService userService;
  private IStandingsService standingsService;
  private List<Game> gameKit;
  private String gameType;
  private LocalTime startTime;

  @Autowired
  public void setGameService(IGameService gameService) {
    this.gameService = gameService;
  }

  @Autowired
  public void setUserService(IUserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setStandingsService(IStandingsService standingsService) {
    this.standingsService = standingsService;
  }

  @RequestMapping(value = "/gameoptions", method = RequestMethod.GET)
  public ModelAndView gamePage(
      @RequestParam(value = "gameType", required = false) String gameType) {
    List<Game> games = gameService.getAllGames();
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("gameOptionsPage");
    modelAndView.addObject("gamesList", games);
    modelAndView.addObject("gameType", gameType);
    return modelAndView;
  }

  @RequestMapping(value = "/game", method = RequestMethod.GET)
  public ModelAndView gameInitPage(
      Principal principal,
      @ModelAttribute(value = "gameType") String testgameType) {
    int userId = userService.getByName(principal.getName()).getId();
    gameKit = gameService.getGameKit(testgameType, userId);
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("gameType", testgameType);
    if (gameKit.size() == 0) {
      modelAndView.setViewName("redirect:/gameoptions");
    } else {
      gameType = testgameType;
      List<String> tempAnswerList = Arrays.asList(new String[gameKit.size()]);
      AnswerList answerList = new AnswerList();
      answerList.setAnswerList(tempAnswerList);
      modelAndView.setViewName("gamePage");
      modelAndView.addObject("gameKit", gameKit);
      modelAndView.addObject("answerList", answerList);
      modelAndView.addObject("time", "00:00:00");
      startTime = LocalTime.now();
    }
    return modelAndView;
  }

  @RequestMapping(value = "/game", method = RequestMethod.POST)
  public ModelAndView gamePage(
      @ModelAttribute("answerList") AnswerList answerList,
      @RequestParam(value = "lose", required = false) String lose) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("gameType", gameType);

    if (lose != null) {
      modelAndView.setViewName("redirect:/gameover");
      return modelAndView;
    }

    List<String> answers = answerList.getAnswerList();
    boolean allCorrect = true;
    for (int i = 0; i < answers.size(); i++) {
      if (!answers.get(i).equals(gameKit.get(i).getAnswerText().get(0))) {
        allCorrect = false;
        break;
      }
    }
    LocalTime startTimer = LocalTime.ofSecondOfDay(
        startTime.until(LocalTime.now(), ChronoUnit.SECONDS));
    String startTimerStr = startTimer.truncatedTo(ChronoUnit.SECONDS).toString();
    modelAndView.addObject("time", startTimerStr);
    if (allCorrect) {
      modelAndView.setViewName("redirect:/gameover");
    } else {
      modelAndView.setViewName("gamePage");
      modelAndView.addObject("gameKit", gameKit);
      modelAndView.addObject("answerList", answerList);
    }
    return modelAndView;
  }

  @RequestMapping(value = "/gameover", method = RequestMethod.GET)
  public ModelAndView gameOverPage(Principal principal, @ModelAttribute("time") String timeStr) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("gameoverPage");
    LocalTime time;
    if (timeStr.equals("")) {
      time = LocalTime.of(20, 0, 0);
      modelAndView.addObject("gameType", gameType);
      modelAndView.addObject("gameKit", gameKit);
    } else {
      time = LocalTime.parse(timeStr);
      modelAndView.addObject("time", timeStr);
    }
    User user = userService.getByName(principal.getName());
    standingsService.create(user, gameKit, time);
    return modelAndView;
  }

}
