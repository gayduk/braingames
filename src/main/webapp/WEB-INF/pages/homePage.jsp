<%@ page session="false" contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
  <title>Welcome</title>
</head>
<body>
<jsp:include page="_menu.jsp" />
	
<div id="main">
  <h3>Добро пожаловать в мир загадок!</h3>
  <p>Здесь вас ждут такие задания как ребусы, кубраи, гибриды и т.д.<br>
    Не знаете, что все это значит? Тогда заходите на вкладку 'Виды заданий', 
    где подробно все описано.<br>
    Заинтересовало? Тогда вводите свой логин и пароль на вкладке 'Login' и начинайте игру!
  </p>
</div>

</body>
</html>