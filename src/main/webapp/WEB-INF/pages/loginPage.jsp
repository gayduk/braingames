<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 
<html>
  <head><title>Login</title></head>
<body>
<jsp:include page="_menu.jsp" />

<div id="main">
  <!-- /login?error=true -->
  <c:if test="${param.error == 'true'}">
    <div style="color:red;">
      Не удалось авторизоваться! <br>
      Причина: ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
    </div>
  </c:if>
  <h4>Введите логин и пароль:</h4>
  <form name='f' action="${pageContext.request.contextPath}/j_spring_security_check" method='POST'>
    <label for="userName">Логин:</label>
    <input type='text' name='username' value=''><br>
    <label for="password">Пароль:</label>
    <input type='password' name='password' /><br>
    <input name="submit" type="submit" value="ОК" />
  </form>
</div>

</body>
</html>