<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Конец игры</title>
</head>

<body>
<jsp:include page="_menu.jsp" />

<div id="main">
  <c:if test="${not empty time}">
    <h3>Поздравляем! Вы справились со всеми заданиями! <br> Ваше время: ${time}</h3>
  </c:if>

  <c:if test="${empty time}">
    К сожалению, Вы ответили правильно не на все задания. Вот полный список правильных ответов:
    <div id="game_kit">    
      <c:set value="/resources/images/${gameType}" var="path"/>
      <c:choose>
        <c:when test="${gameType == 'rebus' || gameType == 'hybrid'}">
          <c:forEach var="game" items="${gameKit}">
            <div class="game_image">
              <img src="<c:url value="${path}/${game.taskReference}"/>" 
                alt="${game.taskReference}" >
              <input type="text" style="border:3px solid green" value="${game.answerText[0]}">
            </div>
          </c:forEach>
        </c:when>		
        <c:otherwise>
          <c:forEach var="game" items="${gameKit}">
            <div class="game_text">
              <label>${game.taskText}</label>
              <input type="text" style="border:3px solid green" value="${game.answerText[0]}">
            </div>
          </c:forEach>
        </c:otherwise>
      </c:choose>
      <div id="footer">
        <a href="<c:url value="/gameoptions"/>">Сыграть новую игру</a>
      </div>
    </div>
  </c:if>

</div>

</body>
</html>