<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <title>Турнирная таблица</title>
</head>
<body>
<jsp:include page="_menu.jsp" />

<div id="main">
  <h3>Турнирная таблица</h3>

  <table>
  <thead>
    <tr>
      <th>Игра</th>
      <th>Пользователь</th>
      <th>Время</th>
    </tr>
  </thead>
  <tbody>
    <c:forEach var="standings" items="${standingsList}">
      <tr>
        <td>${standings.game.type}_${standings.kitNumber}</td>
        <td>${standings.user.userName}</td>
        <c:choose>
          <c:when test="${standings.time == '20:00:00'}">
            <td>сдался</td>
          </c:when>
          <c:otherwise>
            <td>${standings.time}</td>
          </c:otherwise>
        </c:choose>
      </tr>
    </c:forEach>
  </tbody>
  </table>
</div>

</body>
</html>