<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <title>Выбор игры</title>
</head>

<body>
<jsp:include page="_menu.jsp" />

<div id="main">
  <c:url value="/game" var="game"/>
  <h3>Выберите вид игры</h3>

  <c:if test="${not empty gameType}">
    <p><i> К сожалению, вы уже сыграли все возможные игры в разделе ${gameType}. <br> 
      Выберите другой раздел. </i></p>	
  </c:if>

  <form action="${game}" method="GET">
    <input type="hidden" name="gameType" value="rebus">
    <input type="submit" value="РЕБУСЫ">
  </form>

  <form action="${game}" method="GET">
    <input type="hidden" name="gameType" value="kubra">
    <input type="submit" value="КУБРАИ">
  </form>

  <form action="${game}" method="GET">
    <input type="hidden" name="gameType" value="hybrid">
    <input type="submit" value="ГИБРИДЫ">
  </form>

  <form action="${game}" method="GET">
    <input type="hidden" name="gameType" value="hapoifika">
    <input type="submit" value="ГАПОИФИКИ">
  </form>
</div>

</body>
</html>