<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; UTF-8">
    <script src="<c:url value="/resources/js/timer.js"/>" type="text/javascript"></script>
    <title>Игра</title>
</head>

<body onload="startTimer()">
<jsp:include page="_menu.jsp" />

<div id="main">
  <p id="timer">${time}</p>
  <p>Все слова в ответах указываются в нижнем регистре, слитно 
    (без пробелов, деффисов и других небуквенных знаков)</p>
  <form action="<c:url value="/game"/>" method="POST">
    <div id="game_kit">
      <c:choose>
        <c:when test="${gameType == 'rebus' || gameType == 'hybrid'}">
          <c:set value="/resources/images/${gameType}" var="path"/>
          <c:forEach var="answer" items="${answerList.answerList}" varStatus="counter" >
            <div class="game_image">
              <img src="<c:url value="${path}/${gameKit[counter.index].taskReference}"/>" 
                alt="task_${counter.count}" >
              <c:choose>
                <c:when test="${answer == gameKit[counter.index].answerText[0]}">
                  <c:set var="border" value="3px solid green"/>
                </c:when>
                <c:otherwise>
                  <c:set var="border" value="3px solid red"/>
                </c:otherwise>
              </c:choose>
              <input type="text" style="border:${border}" 
                name="answerList[${counter.index}]" value="${answer}"><br>
            </div>
          </c:forEach>
        </c:when>		
        <c:otherwise>
          <c:forEach var="answer" items="${answerList.answerList}" varStatus="counter">
            <div class="game_text">
              <label for="answerList[${counter.index}]">${gameKit[counter.index].taskText}</label>
              <c:choose>
                <c:when test="${answer == gameKit[counter.index].answerText[0]}">
                  <c:set var="border" value="3px solid green"/>
                </c:when>
                <c:otherwise>
                  <c:set var="border" value="3px solid red"/>
                </c:otherwise>
              </c:choose>
              <input type="text" style="border:${border}" 
                name="answerList[${counter.index}]" value="${answer}"><br>
            </div>
          </c:forEach>
        </c:otherwise>
      </c:choose>
      <div id="footer">
        <input type="submit" value="Проверить">
        <input type="submit" value="Сдаюсь" title="Будут показаны правильные ответы" name="lose">
      </div>
    </div>
  </form>
</div>

</body>
</html>