<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 
<html>
<head>
  <title>Пользователи</title>
</head>
<body>
<jsp:include page="_menu.jsp" />

<div id="main">
  <h3>Пользователи</h3>

  <c:url value="/users" var="users"/>
  <table>
  <thead>
    <tr>
      <th>Логин</th>
      <th>Пароль</th>
      <th>Роль</th>
      <th>Действия</th>
    </tr>
  </thead>
  <tbody>
    <c:forEach var="user" items="${usersList}">
      <tr>
        <td>${user.userName}</td>
        <td>${user.password}</td>
        <td>
          <c:forEach var="role" items="${user.roles}">${role}<br></c:forEach>
        </td>
        <c:choose>
          <c:when test="${pageContext.request.userPrincipal.name == user.userName}">
            <td title="Чтобы отредактировать/удалить этого пользователя, 
              необходимо войти под любым другим пользователем-админом">Это вы 
            </td>
          </c:when>
          <c:otherwise>
            <td>
              <a href="${users}/edit/${user.id}"> Изменить </a> /
              <a href="${users}/delete/${user.id}"> Удалить </a>
            </td>            	
          </c:otherwise>
        </c:choose>
      </tr>
    </c:forEach>
    </tbody>
  </table>
  <a href="${users}/add" style="color:#EAE6CA">Добавить нового пользователя</a>
</div>

</body>
</html>