<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
  <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet" type="text/css"/>
  <title>Welcome</title>
</head>

<body>
<div id="header">
  <div id="logo">
    <h1><a href="${pageContext.request.contextPath}/"></a></h1>
  </div>

  <div id="top_menu">
    <ul>
    <li>
        <a href="${pageContext.request.contextPath}/gametypes-descriptions">Виды заданий</a>
    </li>
    <li>
        <a href="${pageContext.request.contextPath}/gameoptions">Игра</a>
	</li>
    <c:choose>
      <c:when test="${pageContext.request.userPrincipal.name == null}">
        <li>
            <a href="${pageContext.request.contextPath}/login">Login</a>
        </li>
      </c:when>
      <c:otherwise>
        <li>
            <a href="${pageContext.request.contextPath}/standings">Турнирная таблица</a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/users">Пользователи</a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/logout">Logout</a>
        </li>
      </c:otherwise>
    </c:choose>        
    </ul>
  </div>

</div>

</body>
</html>