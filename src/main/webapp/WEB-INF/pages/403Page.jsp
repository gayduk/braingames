<%@ page session="false" contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
  <title>Доступ запрещен</title>
</head>
<body>
<jsp:include page="_menu.jsp"/>

<div id="main">
  <h3>${welcome}</h3>
  ${message}
</div>
</body>
</html>