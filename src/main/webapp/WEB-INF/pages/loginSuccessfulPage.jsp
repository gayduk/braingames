<%@ page session="false" contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
  <title>Login</title>
</head>
<body>
<jsp:include page="_menu.jsp" />

<div id="main">
  <h3>Привет, ${pageContext.request.userPrincipal.name}!</h3>
  Теперь ты можешь играть, узнавать результаты уже сыгранных игр, а если ты еще и админ, 
  то редактировать существующих игроков &mdash; просто выбери нужную вкладку меню.
</div>
</body>
</html>