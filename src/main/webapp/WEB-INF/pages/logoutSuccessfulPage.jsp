<%@ page session="false" contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
  <title>Logout</title>
</head>
<body>
<jsp:include page="_menu.jsp" />

<div id="main">
  <h3>Прощай, друг!</h3>
</div>
</body>
</html>