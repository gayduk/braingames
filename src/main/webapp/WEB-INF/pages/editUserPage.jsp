<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<c:choose>
  <c:when test="${empty user.userName}">
    <c:set var="action" value="add"/>
  </c:when>
  <c:otherwise>
    <c:set var="action" value="edit"/>
  </c:otherwise>
</c:choose>
<head>
  <c:if test="${action == 'add'}">
    <title>Добавление пользователя</title>
  </c:if>
  <c:if test="${action == 'edit'}">
    <title>Редактирование пользователя</title>
  </c:if>
</head>
<body>
<jsp:include page="_menu.jsp" />

<div id="main">
  <c:url value="/users/${action}" var="path"/>
  <form action="${path}" method="POST">
    <c:if test="${action == 'edit'}">
      <input type="hidden" name="id" value="${user.id}">
    </c:if>
    <label for="userName">Логин</label>
    <input type="text" name="userName" id="userName" value="${user.userName}">
    <br>
    <label for="password">Пароль</label>
    <input type="text" name="password" id="password" value="${user.password}">
    <br>
    <label for="userRoles">Роли</label> <br>
   	<input type="checkbox" name="userRoles" value="USER" CHECKED onclick="this.checked=true"/>
      <span style="color:#654321"> USER </span><br>	
   	<input type="checkbox" name="userRoles" value="ADMIN"/>
      <span style="color:#654321"> ADMIN </span><br>
    <input type="submit" value="Сохранить">
</form>
</div>

</body>
</html>