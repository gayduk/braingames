function startTimer() {
    var timer = document.getElementById("timer");
    var time = timer.innerHTML;
    var arr = time.split(":");
    var hh = arr[0];
    var mm = arr[1];
    var ss = arr[2];
    
    ss++;
    if (ss == 60){
    	ss = "00";
    	mm++;
    	if (mm == 60){
    		mm = "00";
    		hh++;
    		if (hh < 10){ 
            	hh = "0" + hh;
    		}
    	}
    	else if (mm < 10){ 
    		mm = "0" + mm;
    	}
    }
    else if (ss < 10){
        ss = "0" + ss;
    }    
    document.getElementById("timer").innerHTML = hh+":"+mm+":"+ss;
    setTimeout(startTimer, 1000);
}